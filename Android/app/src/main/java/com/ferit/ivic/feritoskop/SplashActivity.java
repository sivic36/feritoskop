package com.ferit.ivic.feritoskop;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        final ImageView myImageView= (ImageView)findViewById(R.id.splash);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Animation myFadeInAnimation1 = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_in);
                myImageView.startAnimation(myFadeInAnimation1);
            }
        }, 100);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Animation myFadeInAnimation2 = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_out);
                myImageView.startAnimation(myFadeInAnimation2);
            }
        }, 1500);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, 2000);
    }
}
