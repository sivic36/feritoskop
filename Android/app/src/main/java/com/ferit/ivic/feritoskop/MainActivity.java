package com.ferit.ivic.feritoskop;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import java.text.DecimalFormat;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;

import com.ferit.ivic.feritoskop.Bluetooth;

public class MainActivity extends AppCompatActivity
		implements View.OnClickListener{

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Bluetooth.connectedThread != null) {
			tbRun.setChecked(false);
			Bluetooth.connectedThread.write("A");}//Stop streaming
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		if (Bluetooth.connectedThread != null) {
			tbRun.setChecked(false);
			Bluetooth.connectedThread.write("A");}//Stop streaming
		super.onPause();
	}

	static boolean Trigger;
	static boolean Start;
	static boolean AutoScrollX;//auto scroll to the last x value
	static boolean ACMode;
	//Button init
	Button Xminus;
	Button Xplus;
	LinearLayout background;

	static DecimalFormat df2 = new DecimalFormat("#0.00");
	DecimalFormat df3 = new DecimalFormat("#0.000");

	TextView volts, secs;
	static TextView freq, dV, dt, trigLevel;
	static int[] xBase = new int[] {2,10,20,50,100,200,500};//vremenska os
	static int[] xBaseLabel = new int[] {2,10,20,50,100,200,500}; //vremenski podioci (os x 2ms)
	static int i=4;
	private static int Xview= xBase[i];
	Button Yminus;
	Button Yplus;
	static int j=4;
	static int yScale[]= new int[] {41, 82, 205, 410, 820, 1640, 2460, 4100};
	static int yScaleLabel[]= new int[] {50, 100, 250, 500, 1, 2, 3, 5};
	static int Ymax;
	static int Ymin;

	ToggleButton tbTrigger;
	ToggleButton tbRun;
	Button bConnect, bDisconnect;
	DrawerLayout drawer;
	FloatingActionButton fab;
	CheckBox cb;
	RadioGroup rg;

	static void calculateTimeCursors(){
		float dtime = Math.abs(graphView.cursor2.value-graphView.cursor1.value);
		dt.setText("dt: " + df2.format(dtime) + " ms");
		freq.setText(df2.format(1000/(dtime)) + " Hz");
	}

	static void calculateVoltsCurors(){
		float dVolts = Math.abs(graphView.cursorA.value - graphView.cursorB.value);
		dV.setText("dV: " + df2.format(dVolts) + " mV");

	}

	LinearLayout GraphViewLayout;
	LinearLayout LL2;
	LinearLayout cursorsLayout;
	TextView CH1max, CH1min;
	ImageView trigUp, trigDown;

	static GraphView graphView;
	static GraphViewSeries Series;
	private static double lastXValue = 0;

	GraphView.GraphViewData[] trace=new GraphView.GraphViewData[10*xBase[4]+1];
	boolean f=true;
	boolean trigFlag=false;
	boolean triggerMode;
	boolean triggerInAction;
	float triggerLevel;
	int index=0;

	int inVoltage, inVoltage2;
	byte[] readBuf;

	public static String[] horLabels={"","","","","","","","","","",""};
	public static String[] verLabels={"","","","","","","","",""}; // veličine YDivsNumber

	@SuppressLint("HandlerLeak")
	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			/* TODO Auto-generated method stub */
			super.handleMessage(msg);
			switch(msg.what){
			case Bluetooth.SUCCESS_CONNECT:
				Bluetooth.connectedThread = new Bluetooth.ConnectedThread((BluetoothSocket)msg.obj);
				Toast.makeText(getApplicationContext(), "Connected!", Toast.LENGTH_SHORT).show();
				String s = "successfully connected";
				Bluetooth.connectedThread.start();
				Bluetooth.connectedThread.write("A");
				bConnect.setVisibility(View.GONE);
				bDisconnect.setVisibility(View.VISIBLE);

			break;
			case Bluetooth.MESSAGE_READ:
				readBuf = (byte[]) msg.obj;
				//char low;
				//char high;
				int readBuflength=msg.arg1;
				for(int i=0; i<readBuflength; i++){

					inVoltage = (readBuf[i]<<2);

					if(i>0) inVoltage2= (readBuf[i-1]<<2);
					else inVoltage2=Integer.MAX_VALUE;

					triggerLevel = graphView.trigger.value;
					triggerMode = graphView.trigger.triggerMode;

					if(Trigger && Xview<=xBase[4]){

						graphView.setViewPort(0, Xview);

						if(triggerMode){
							if(inVoltage>triggerLevel && inVoltage2<triggerLevel){
								trigFlag=true;
								triggerInAction=true;}}
						else{
							if(inVoltage<triggerLevel && inVoltage2>triggerLevel) {
								trigFlag=true;
								triggerInAction=true;}}

						if(lastXValue !=0 && f){
							Series.resetData(new GraphView.GraphViewData[]{});
							lastXValue =0;
							graphView.setViewPort(0,Xview);
							index=0;
							f=false;
						}

						if(trigFlag){
							trace[index]=new GraphView.GraphViewData(lastXValue,inVoltage);
							graphView.drawTrigger=false;
							if(index >= 10*Xview) {
								Series.resetData(new GraphView.GraphViewData[]{});
								for(index=0;index<trace.length;index++)
									Series.appendData(trace[index],AutoScrollX);
								index=0;
								lastXValue =0;
								f=false;
								trigFlag=false;

								if(Series.values.length>1) {
									CH1max.setText("Max: " + df2.format((graphView.getMaxYValue() / 1023) * 5 * 1000)+ " mV");
									CH1min.setText("Min: " + df2.format((graphView.getMinYValue() / 1023) * 5 * 1000) + " mV");
								}

							}
							else {
								index++;
								lastXValue += 0.1;
							}
						}
					}
					else if(!Trigger){
						if(!f){
							f=true;
							lastXValue =Xview;
						}
						else {
							graphView.setViewPort(0, Xview);
							if (lastXValue>Xview){
								lastXValue=0;
								Series.resetData(new GraphView.GraphViewData[]{});
							}
							else{
								Series.appendData(new GraphView.GraphViewData(lastXValue, inVoltage), AutoScrollX);
								lastXValue += 0.1;
							}
							graphView.refreshGraph();
						}
					}
					else if(Trigger && Xview>xBase[4]){
						Series.resetData(new GraphView.GraphViewData[]{});
						lastXValue =0;
						Trigger =false;
						tbTrigger.setChecked(false);
						graphView.cursorToMove= GraphView.CursorToMove.graph;
					}
					if(Bluetooth.connectedThread == null)
						reset();
				}
				break;
			}
		}

	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);  // No action bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);   //No status bar
		setContentView(R.layout.options_main);
		background = findViewById(R.id.bg);
		background.setBackgroundColor(Color.WHITE);
		volts=findViewById(R.id.volts);
		secs=findViewById(R.id.secs);
		secs.setText(xBaseLabel[i]+"ms");
		init();
		ButtonInit();
	}

	void init(){
		Bluetooth.gethandler(mHandler);
		//inicijalizacija grafa
		GraphViewLayout = findViewById(R.id.Graph);
		LL2=findViewById(R.id.LL2);
		GraphViewLayout.setBackgroundColor(Color.BLACK);
		// init example series data-------------------
		Series = new GraphViewSeries("Signal", 
				new GraphViewSeries.GraphViewStyle(Color.rgb(255, 220, 10), 4),
				new GraphView.GraphViewData[] {new GraphView.GraphViewData(0, 0)});
		graphView = new LineGraphView(
				this, "" );
		graphView.setViewPort(0, Xview);
		graphView.setScrollable(true);
		graphView.setShowLegend(true);
		graphView.setLegendWidth(100F);
		graphView.setLegendAlign(com.ferit.ivic.feritoskop.GraphView.LegendAlign.BOTTOM);
		graphView.setManualYAxis(true);
		graphView.addSeries(Series); // data
		graphView.setVerticalLabels(verLabels);
		graphView.setHorizontalLabels(horLabels);
		GraphViewLayout.addView(graphView);
		initYscale(j);
	}

	void ButtonInit(){

		bConnect = (Button)findViewById(R.id.bConnect);
		bConnect.setOnClickListener(this);
		bDisconnect = (Button)findViewById(R.id.bDisconnect);
		bDisconnect.setOnClickListener(this);

		Yminus = (Button)findViewById(R.id.Yminus);
		Yminus.setOnClickListener(this);
		Yplus = (Button)findViewById(R.id.Yplus);
		Yplus.setOnClickListener(this);
		Xminus=findViewById(R.id.Xminus);
		Xplus=findViewById(R.id.Xplus);
		Xminus.setOnClickListener(this);
		Xplus.setOnClickListener(this);

		tbTrigger = (ToggleButton)findViewById(R.id.trigger);
		tbTrigger.setOnClickListener(this);
		tbTrigger.setChecked(false);
		tbRun = (ToggleButton)findViewById(R.id.tbRun);
		tbRun.setOnClickListener(this);
		tbRun.setChecked(false);Start=false;
		//init toggleButton
		Trigger =false;
		AutoScrollX=false;
		ACMode=false;

		fab = findViewById(R.id.fab);
		fab.setOnClickListener(this);
		drawer = findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = findViewById(R.id.nav_view);
		//navigationView.setNavigationItemSelectedListener(this);

		rg=findViewById(R.id.rg1);
		cb=findViewById(R.id.showCursors);
		cb.setOnClickListener(this);

		rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId){
					case R.id.cursorA:
						graphView.cursorToMove= GraphView.CursorToMove.cursorA;
						break;
					case R.id.cursorB:
						graphView.cursorToMove= GraphView.CursorToMove.cursorB;
						break;
					case R.id.cursor1:
						graphView.cursorToMove= GraphView.CursorToMove.cursor1;
						break;
					case R.id.cursor2:
						graphView.cursorToMove= GraphView.CursorToMove.cursor2;
						break;
				}}});

		dt=findViewById(R.id.dt);
		dV=findViewById(R.id.dV);
		freq=findViewById(R.id.freq);
		trigLevel=findViewById(R.id.trigLevel);
		cursorsLayout=findViewById(R.id.cursorsLayout);

		trigUp=findViewById(R.id.trigRising);
		trigDown=findViewById(R.id.trigFalling);

		trigUp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				graphView.trigger.triggerMode=true;
				trigUp.setImageResource(R.drawable.trigmode_upselected);
				trigDown.setImageResource(R.drawable.trigmode_down);
			}
		});

		trigDown.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				graphView.trigger.triggerMode=false;
				trigUp.setImageResource(R.drawable.trigmode_up);
				trigDown.setImageResource(R.drawable.trigmode_downselected);
			}
		});

		CH1max=findViewById(R.id.CH1max);
		CH1min=findViewById(R.id.CH1min);
	}

	public void reset(){
		GraphViewLayout.removeView(graphView);
        lastXValue = 0;
        Series.resetData(new GraphView.GraphViewData[]{new GraphView.GraphViewData(0,0)});
        GraphViewLayout.addView(graphView);
        graphView.refreshGraph();
		CH1max.setText("Max: ");
		CH1min.setText("Min: ");
    }

	protected void setYscale(int j){
		if(ACMode){}
		else{
			int d;
			d=yScale[j];
			GraphViewLayout.removeView(graphView);
			Ymax = d;
			Ymin = -d;
			graphView.setManualYAxisBounds(Ymax, Ymin);
			GraphViewLayout.addView(graphView);
			graphView.refreshGraph();
			if(j>=4) volts.setText("CH1: "+ yScaleLabel[j] +"V");
			else volts.setText("CH1: "+ yScaleLabel[j] +"mV");
		}
	}

	protected void initYscale(int j){
		if(ACMode){}
		else{
			int d;
			d=yScale[j];
			GraphViewLayout.removeView(graphView);
			Ymax = d;
			Ymin = -d;
			graphView.setManualYAxisBounds(Ymax, Ymin);
			GraphViewLayout.addView(graphView);
			if(j>=4) volts.setText("CH1: "+ yScaleLabel[j] +"V");
			else volts.setText("CH1: "+ yScaleLabel[j] +"mV");
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.bConnect:
			startActivity(new Intent("android.intent.action.Bluetooth"));
			break;
			case R.id.bDisconnect:
				tbRun.setChecked(false);
				Start=false;
				if (Bluetooth.connectedThread != null) Bluetooth.connectedThread.write("A");
				reset();
				Bluetooth.disconnect();
				bDisconnect.setVisibility(View.GONE);
				bConnect.setVisibility(View.VISIBLE);
			break;
			case R.id.Yminus:
			if (j>0){
				j--;
				setYscale(j);
			}
			break;
			case R.id.Yplus:
				if (j<7){
					j++;
					setYscale(j);
				}
				break;
			case R.id.Xplus:
				if (i<6){
					i++;
					GraphViewLayout.removeView(graphView);
					Xview= xBase[i];
					f=true;
					trace=new GraphView.GraphViewData[10*Xview+1];
					graphView.setViewPort(0,Xview);
					GraphViewLayout.addView(graphView);
					graphView.refreshGraph();
					if(i>=7)secs.setText(xBaseLabel[i]+"s");
					else secs.setText(xBaseLabel[i]+"ms");
				}
				break;
			case R.id.Xminus:
				if (i>0){
					i--;
					GraphViewLayout.removeView(graphView);
					Xview= xBase[i];
					trace=new GraphView.GraphViewData[10*Xview+1];
					f=true;
					graphView.setViewPort(0,Xview);
					GraphViewLayout.addView(graphView);
					graphView.refreshGraph();
					if(i>=7)secs.setText(xBaseLabel[i]+"s");
					else secs.setText(xBaseLabel[i]+"ms");
				}
				break;
		case R.id.trigger:
			if (tbTrigger.isChecked()){
				Trigger = true;
				graphView.drawTrigger=true;
				graphView.cursorToMove= GraphView.CursorToMove.trigger;
				graphView.trigger.getTriggerValue(graphView.canvasHeight);
				GraphViewLayout.removeView(graphView);
				GraphViewLayout.addView(graphView);
				graphView.refreshGraph();
			}
			else{
				Trigger = false;
				graphView.drawTrigger=false;
				graphView.cursorToMove= GraphView.CursorToMove.graph;
				GraphViewLayout.removeView(graphView);
				GraphViewLayout.addView(graphView);
				graphView.refreshGraph();
			}
			break;
		case R.id.tbRun:
			if (tbRun.isChecked()){

				if (Bluetooth.connectedThread != null){
					Bluetooth.connectedThread.write("w");
					Bluetooth.connectedThread.write("w");
					Start=true;
					Bluetooth.stopFlag=false;
				}
				else{
					tbRun.setChecked(false);
					Toast.makeText(getApplicationContext(), "You're not connected!", Toast.LENGTH_SHORT).show();
				}

			}else{

				if (Bluetooth.connectedThread != null){
					Bluetooth.connectedThread.write("A");
					Bluetooth.connectedThread.write("A");
					Start=false;
					Bluetooth.stopFlag=true;
				}
			}
			break;
			case R.id.fab:
				if (drawer.isDrawerOpen(Gravity.END)) {
					drawer.closeDrawer(Gravity.END);
				}
				else {
					drawer.openDrawer(Gravity.END);
				}

				if(Series.values.length>1) {
					CH1max.setText("Max: " + df2.format((graphView.getMaxYValue() / 1023) * 5 * 1000)+ " mV");
					CH1min.setText("Min: " + df2.format((graphView.getMinYValue() / 1023) * 5 * 1000) + " mV");
				}
				else{
					CH1max.setText("Max: ");
					CH1min.setText("Min: ");
				}

				break;
			case R.id.showCursors:
				if(cb.isChecked()){
					graphView.drawCursors=true;
					GraphViewLayout.removeView(graphView);
					GraphViewLayout.addView(graphView);
					graphView.cursorToMove = GraphView.CursorToMove.graph;
					graphView.drawTrigger=false;
					rg.clearCheck();
					cursorsLayout.setVisibility(View.VISIBLE);
					graphView.refreshGraph();
				}
				else{
					graphView.drawCursors=false;
					GraphViewLayout.removeView(graphView);
					GraphViewLayout.addView(graphView);
					rg.clearCheck();
					graphView.cursorToMove = GraphView.CursorToMove.graph;
					cursorsLayout.setVisibility(View.GONE);
					graphView.refreshGraph();
				}
				break;
		}
	}
}
