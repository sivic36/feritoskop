package com.ferit.ivic.feritoskop;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GraphViewSeries
{
  final String description;
  final GraphViewStyle style;
  GraphView.GraphViewData[] values;
  
  public static class GraphViewStyle
  {
    public int color = -16746548;
    public int thickness = 3;
    
    public GraphViewStyle() {}
    
    public GraphViewStyle(int color, int thickness)
    {
      this.color = color;
      this.thickness = thickness;
    }
  }

  private final List<GraphView> graphViews = new ArrayList();
  
  public GraphViewSeries(Context context, GraphView.GraphViewData[] values) {
    description = null;
    style = new GraphViewStyle();
    this.values = values;
  }
  
  public GraphViewSeries(String description, GraphViewStyle style, GraphView.GraphViewData[] values)
  {
    this.description = description;
    if (style == null) {
      style = new GraphViewStyle();
    }
    this.style = style;
    this.values = values;
  }


  public void addGraphView(GraphView graphView)
  {
    graphViews.add(graphView);
  }

  @TargetApi(9)
  public void appendData(GraphView.GraphViewData value, boolean scrollToEnd)
  {
    GraphView.GraphViewData[] newValues = (GraphView.GraphViewData[])Arrays.copyOf(values, values.length + 1);
    newValues[values.length] = value;
    values = newValues;
    for (GraphView g : graphViews) {
      if (scrollToEnd) {
        g.scrollToEnd();
      }
    }
  }

  public void resetData(GraphView.GraphViewData[] values)
  {
    this.values = values;
    for (GraphView g : graphViews) {
      g.redrawAll();
    }
  }
}
