package com.ferit.ivic.feritoskop;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class ScaleGestureDetector
{
  private Object realScaleGestureDetector;
  private Method method_getScaleFactor;
  private Method method_isInProgress;
  private Method method_onTouchEvent;
  
  public ScaleGestureDetector(Context context, SimpleOnScaleGestureListener simpleOnScaleGestureListener)
  {
    try
    {
      Class.forName("android.view.ScaleGestureDetector");
      

      Class<?> classRealScaleGestureDetector = Class.forName("com.jjoe64.graphview.compatible.RealScaleGestureDetector");
      method_getScaleFactor = classRealScaleGestureDetector.getMethod("getScaleFactor", new Class[0]);
      method_isInProgress = classRealScaleGestureDetector.getMethod("isInProgress", new Class[0]);
      method_onTouchEvent = classRealScaleGestureDetector.getMethod("onTouchEvent", new Class[] { MotionEvent.class });
      

      Constructor<?> constructor = classRealScaleGestureDetector.getConstructor(new Class[] { Context.class, getClass(), SimpleOnScaleGestureListener.class });
      realScaleGestureDetector = constructor.newInstance(new Object[] { context, this, simpleOnScaleGestureListener });
    }
    catch (Exception e) {
      Log.w("com.jjoe64.graphview", "*** WARNING *** No scaling available for graphs. Exception:");
      e.printStackTrace();
    }
  }
  
  public double getScaleFactor() {
    if (method_getScaleFactor != null) {
      try {
        return ((Float)method_getScaleFactor.invoke(realScaleGestureDetector, new Object[0])).floatValue();
      } catch (Exception e) {
        e.printStackTrace();
        return 1.0D;
      }
    }
    return 1.0D;
  }
  
  public boolean isInProgress() {
    if (method_getScaleFactor != null) {
      try {
        return ((Boolean)method_isInProgress.invoke(realScaleGestureDetector, new Object[0])).booleanValue();
      } catch (Exception e) {
        e.printStackTrace();
        return false;
      }
    }
    return false;
  }
  
  public void onTouchEvent(MotionEvent event) {
    if (method_onTouchEvent != null) {
      try {
        method_onTouchEvent.invoke(realScaleGestureDetector, new Object[] { event });
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  public static abstract interface SimpleOnScaleGestureListener
  {
    public abstract boolean onScale(ScaleGestureDetector paramScaleGestureDetector);
  }
}
