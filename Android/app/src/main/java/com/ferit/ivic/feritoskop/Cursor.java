package com.ferit.ivic.feritoskop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import static com.ferit.ivic.feritoskop.MainActivity.Ymax;
import static com.ferit.ivic.feritoskop.MainActivity.Ymin;
import static com.ferit.ivic.feritoskop.MainActivity.i;
import static com.ferit.ivic.feritoskop.MainActivity.xBaseLabel;


public class Cursor {

    Bitmap bmp;
    boolean orientation; // true-horizontal (A,B)   false-vertical (1,2)
    float cursorLevel;
    float value;
    boolean triggerMode; //true for rising, false for falling edge

    public Cursor(Context context, int resId, boolean or, float cursorLvl) {
        orientation = or;
        Drawable myDrawable = context.getResources().getDrawable(resId);
        bmp = ((BitmapDrawable) myDrawable).getBitmap();
        cursorLevel=cursorLvl;
        triggerMode=true; //rising
    }

    public void setValue(float size) {
        if(orientation){ //za horizontalne kursore A i B (dV)
            value=Ymax-(cursorLevel/size)*(Ymax-Ymin);
            value=value*5000/1023; //u mV
        }
        else{ //za vertikalne kursore 1 i 2 (dt)
            int timelineLength=xBaseLabel[i]*10; // duljina vremenske osi u ms
            value=(cursorLevel/size)*timelineLength;
            //vrijeme u ms
        }
    }

    public float getTriggerValue(float size) {
        value=Ymax-(cursorLevel/size)*(Ymax-Ymin);
        return value;
    }

}
