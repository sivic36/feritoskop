# <img src="Pictures/feritoskop-logo.png" title="Bugsy logo" width="255" height="45">

## Android wireless oscilloscope with Arduino microprocessor

### [Feritoskop](http://feritoskop.gitlab.io) website - check it out

### What is Feritoskop?

**Feritoskop** is a device, built within [Pro-student](http://pro-student.ferit.hr/) project from 2018, that represents a wireless oscilloscope with a corresponding **Android** application that is adapted to use on a tablet. The signal to be analyzed is sent to the device with the **Arduino** microprocessor (hardware part of the Feritoskop) that processes it and displays it via the Bluetooth serial communication on the tablet device using the Feritoskop Android application (software part of the project). The application provides the user with the most important function for signal display and analysis as well as for ordinary oscilloscope.

### Screenshots

<img src="Pictures/feritoskop-tablet.png" title="Feritoskop" height="350">

### Comment

Android app was developed using `Android Studio` and the Arduino part was done using `Arduino IDE`.
