#include <SoftwareSerial.h>
#include <TimerOne.h>

#define sampling_time 1000 //us
#define k 1.126

SoftwareSerial bt(10, 11); // rx, tx

volatile int i;
volatile bool uzorkovano;

void setup() 
{
  bt.begin(57600);
  uzorkovano=false;
  standBy();
  Timer1.initialize(sampling_time); //us
  Timer1.attachInterrupt(uzorak);
}

void uzorak() 
{
  i=(int)(k*analogRead(A0));
  uzorkovano = true;
}

void loop() 
{
    if(bt.read()=='A'){standBy();}
    else
    {
      if(uzorkovano) 
      {
        uzorkovano = false;
        bt.write(i>>2);
      }
    }
}

void standBy() 
{
  while(1)
  {
      if(bt.read()=='w'){break;}
  }
}
