<?php

    $host="localhost";
    $user="root";
    $pass="";
    $db="feritoskop";

    // Create connection
    $conn=mysqli_connect($host,$user,$pass,$db);
    
    // Set UTF-8 charset when retrieving data from MySQL database
    mysqli_set_charset($conn,"utf8");

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

?>