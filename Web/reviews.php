<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/feritoskop.ico">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <title>Feritoskop</title>

    <!-- Main css -->
    <link href="css/carousel.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Themify Icons -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    
  </head>

  <body>

    <header>

      <!--NAVIGATION BAR-->
      <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white">
        <img class="img-logo" href="index.php" src="img/feritoskop-logo.png">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.php">About</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="reviews.php">Reviews</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <main role="main">

      <!-- Reviews cover-->
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/bg-image.png" alt="Feritoskop background">
            <div class="container">
              <div class="carousel-caption text-right" style="background-color: #00223e5b; padding: 1rem; border-radius: 5px;">
                <p>Home -> Reviews</p>
                <h1>Discover Feritoskop</h1>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container marketing">
        <!--REVIEWS!------>

        <div class="section">
          <div class="container">
              <div class="section-title">
                  <small>USER FEEDBACK</small>
                  <h1 >What do Feritoskop users say?<div class="text-muted"> Take a look...</div></h1>
              </div>
              <div class="testimonials owl-carousel" style="margin-top: 3rem">
				<!-- PHP part -->
              </div>
          </div>
        </div>
        <!-- // end .section -->

        <hr class="featurette-divider">

        <!--REVIEW FORM -->

        <div class="container">
          <div class="row">
          <h1 >Review!<span class="text-muted"> Please give us your feedback.</span></h1>
          <table width="100%" style="margin-top: 1.5rem">
            <div class="col-md-9 col-md-offset-0">
              <tr><td width="77%">
              <div class="form-container">

                <form id="review-form" method = "post">

                  <fieldset>
                    <!-- Name input-->
                    <div class="form-group">
                      <label class="col-md-3 control-label" for="name">Your Name</label>
                      <div class="col-md-9">
                        <input id="name" name="name" type="text" placeholder="Enter your full name" class="form-control" required="required">
                      </div>
                    </div>
            
                    <!-- Email input-->
                    <div class="form-group">
                      <label class="col-md-3 control-label" for="email">Your E-mail</label>
                      <div class="col-md-9">
                        <input id="email" name="email" type="email" placeholder="Enter your e-mail" class="form-control" required="required">
                      </div>
                    </div>
            
                    <!-- Message body -->
                    <div class="form-group">
                      <label class="col-md-3 control-label" for="message">Your Message</label>
                      <div class="col-md-9">
                        <textarea class="form-control" id="message" name="message" placeholder="Please enter your feedback here..." rows="2" required="required"></textarea>
                      </div>
                    </div>

                    <!-- Rating -->
                    <div class="form-group" id="rating-ability-wrapper">
                    <label class="col-md-5 control-label" for="message">How would You rate Feritoskop?</label>
                      <span class="field-label-info"></span>
                      <input type="hidden" id="selected_rating" name="selected_rating" value="" required="required">
                      </label>
                      <div class="col-md-9">
                        <h2 class="bold rating-header" style="">
                        <span class="selected-rating">0</span><small> / 5</small>
                        </h2>
                        <button type="button" class="btnrating btn btn-default btn-lg" data-attr="1" id="rating-star-1">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btnrating btn btn-default btn-lg" data-attr="2" id="rating-star-2">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btnrating btn btn-default btn-lg" data-attr="3" id="rating-star-3">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btnrating btn btn-default btn-lg" data-attr="4" id="rating-star-4">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btnrating btn btn-default btn-lg" data-attr="5" id="rating-star-5">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>

                    <!-- Form actions -->

                    <div class="form-group submit-container" style="margin-top: 3rem">
                      <div class="col-md-9">
                        <button id="submit" type="submit" class="btn btn-primary btn-md mr-2">Submit</button>
                        <button id="clear" type="reset" class="btn btn-default btn-md">Clear</button>
                        <span id="error_message" class="text-danger form-message"></span>  
                        <span id="success_message" class="text-success form-message"></span> 
                      </div>
                    </div>

                  </fieldset>
                </form>
              </div>
            </div>
            </td>
            </tr>
            </table>
        </div>
        <!-- REVIEW FORM END -->
      </div>

      <!-- FOOTER -->
      <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017-2018 FERITOSKOP</p>
      </footer>

    </main>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js"></script>
    <script src="js/rating.js"></script>
    <script src="js/review.js"></script>
    <!-- Plugins JS -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- Custom JS -->
    <script src="js/main.js"></script>
  </body>
</html>
