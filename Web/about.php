<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/feritoskop.ico">

    <title>Feritoskop</title>

    <link href="css/carousel.css" rel="stylesheet">

    <link href="css/bootstrap.min.css" rel="stylesheet">

  </head>

  <body>

    <header>

      <!--NAVIGATION BAR-->
      <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white">
        <img class="img-logo" href="index.php" src="img/feritoskop-logo.png">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="about.php">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="reviews.php">Reviews</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <main role="main">

      <!--About cover-->
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/bg-image.png" alt="Feritoskop background">
            <div class="container">
              <div class="carousel-caption text-right" style="background-color: #00223e5b; padding: 1rem; border-radius: 5px;">
                <p>Home -> About</p>
                <h1>Meet Feritoskop</h1>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Specs -->

      <div class="container marketing">

        <div class="col-md-7">
          <h2 class="featurette-heading">Specifications</h2>
        </div>

        <hr class="featurette-divider">

        <div class="row">
          <div class="col-md-7">
            <h2 class="featurette-heading">How about some numbers? <span class="text-muted">Here you go...</span></h2>
            
            <p class="lead"><br><b>Feritoskop</b> can sample the measured signal with the frequency of 200 Hz and thus, the recommended
            input signal frequency must be less than <b>100 Hz</b>. Input voltage is limited to range: <b>0-5 V</b> due to
            Arduino input regulation. In the case of negative voltage, Feritoskop has its own protection using resistor and 
            two Schottky diodes.<br>
            Arduino Uno microprocessor is <b>ATmega328P</b> and the Bluetooth module used is <b>HC-05</b> with range to about <b>10m</b>.
            </p>
          </div>
          <div class="col-md-5">
            <img class="img-fluid mx-auto" src="img/feritoskop-hausing.png" alt="Feritoskop image">
            <p></p>
            <img class="img-fluid mx-auto" src="img/feritoskop-hardware.png" alt="Feritoskop image">
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row">
          <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">Scheme? <span class="text-muted">As simple as it gets.</span></h2>
            <p class="lead"><br>The hardware part of the project consists of Arduino Uno development boards with 
            <b>ATmega328P</b> microprocessor, power supply, switch, Bluetooth module for Arduino -> <b>HC-05</b>, BNC connectors, 
            several resistors and experimental plates and cables for connecting all parts of the device.</p>
          </div>
          <div class="col-md-5 order-md-1">
            <img class="featurette-image img-fluid mx-auto" src="img/scheme.png" alt="Feritoskop image">
            <p></p>
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row">
          <div class="col-md-7">
            <h2 class="featurette-heading">Housing? <span class="text-muted">3D printed!</span></h2>
            <p class="lead"><br>The housing of the Feritoscope is a <b>3D-printed</b> case model created in the 
            <b>Autodesk Fusion 360</b> software package. The housing, corresponding to the system requirements, 
              consists of three parts: the lower part (power supply), the middle or main part with all electronics
               and the upper part - the lid. The lower part is a power supply unit and contains the necessary elements 
               to accommodate <b>6 x Ni-Mh batteries</b>, voltage <b>1.2V (± 0.03V)</b>, which are serially 
               connected to provide a <b>7.2V</b> output voltage sufficient for powering the Arduino plate 
               (recommended voltage power supply: <b>7-12V, limit: 6-20V</b>) and the ignition / shutdown switch.</p>
          </div>
          <div class="col-md-5">
            <img class="img-fluid mx-auto" src="img/print3d2.png" alt="Feritoskop image">
            <p></p>
            <img class="img-fluid mx-auto" src="img/print3d.jpg" alt="Feritoskop image">
          </div>
        </div>

        <hr class="featurette-divider">

        <div>
          <h2 class="featurette-heading" style="margin-bottom: 2.5rem">Dimensions</h2>
        </div>
        <div>
          <img class="img-fluid mx-auto" src="img/dimensions.png" alt="Feritoskop image">
        </div>

        <!-- /END THE SPECS -->

        <hr class="featurette-divider" id="gitlab">

        <div class="row">
          <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">Feritoskop is open source! <span class="text-muted">Check us on GitLab!</span></h2>
            <p class="lead"><br></p>
            <a class="btn btn-lg btn-primary mt-2 mb-2" href="https://gitlab.com/sivic36/Feritoskop" role="button">GitLab repository</a>
          </div>
          <div class="col-md-5 order-md-1 mt-2">
            <img class="featurette-image img-fluid mx-auto" src="img/gitlab-logo.png" alt="Gitlab image">
            <p></p>
          </div>
        </div>

        <hr class="featurette-divider">

        <!--TEAM-->

        <h2 class="featurette-heading" style="margin-bottom: 2.5rem">Feritoskop team. <span class="text-muted">Who are we?</span></h2>

        <!-- Three columns of text below the carousel -->
        <div class="row">
          <div class="col-lg-4">
            <img  class="rounded-circle" src="img/team1.jpg" alt="team1" width="140" height="140">
            <h2>Slaven Ivić</h2>
            <p>Student at FERIT</p>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <img class="rounded-circle" src="img/team2.jpg" alt="team2" width="140" height="140">
            <h2>Anamarija Blavicki</h2>
            <p>Student at FERIT</p>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <img class="rounded-circle" src="img/team3.jpg" alt="team3" width="140" height="140">
            <h2>Vedran Ivić</h2>
            <p>Student at FERIT</p>
          </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->

      </div><!-- /.container -->

      <!-- FOOTER -->
      <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017-2018 FERITOSKOP</p>
      </footer>

    </main>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js"></script>
  </body>
</html>
