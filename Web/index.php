<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/feritoskop.ico">

    <title>Feritoskop</title>

    <link href="css/carousel.css" rel="stylesheet">

    <link href="css/bootstrap.min.css" rel="stylesheet">

  </head>

  <body>

    <header>
        <!--NAVIGATION BAR-->
        <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white">
          <img id="logo" class="img-logo" href="index.php" src="img/feritoskop-logo.png" style="width: 0; opacity: 0">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="reviews.php">Reviews</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
            </ul>
          </div>
        </nav>
    </header>

    <main role="main">

      <!--HOME CAROUSEL-->
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item home active">
            <img src="img/fskop1.jpg" alt="First slide">
            <div class="container">
              <div class="carousel-caption text-left">
                <img src="img/feritoskop-logo-sm.png" height="160">
                <h1>Feritoskop</h1>
                <p>Android wireless oscilloscope powered by Arduino Uno R3 microprocessor</p>
                <p>
                  <a class="btn btn-lg btn-primary mr-2" href="https://drive.google.com/open?id=1cHqi4A1ldKwO0BL-7YnvHg8CHyvZ1QUV" role="button">Download the App</a>
                </p>
              </div>
            </div>
          </div>
          <div class="carousel-item home">
            <img src="img/fskop2.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <img src="img/pro-student-logo.png" height="120">
                <h1>Pro-student project</h1>
                <p>Feritoskop is a student project developed within <a href="https://www.ferit.unios.hr/">FERIT</a>-
                  Faculty of Electrical Engineering, Computer Science and Information Technology in Osijek, Croatia</p>
                <p><a class="btn btn-lg btn-primary" href="http://pro-student.ferit.hr/" role="button">Pro-student</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item home">
            <img class="third-slide" src="img/bg-slide.png" alt="Third slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <img src="img/tablets.png" style="width: 100%; height: 100%; max-height: 17rem; max-width: 30rem">
                <h1>Open source</h1>
                <p>Feritoskop is open source! You can find all the information in About section and
                  if you're interested in contribution, check out our GitLab repository</p>
                <p><a class="btn btn-lg btn-primary" href="about.php#gitlab" role="button">Learn more</a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

      <!-- FEATURES -->

      <div class="container marketing">

        <!-- Three columns of text below the carousel -->
        <div class="row">
          <div class="col-lg-4">
            <img  src="img/bluetooth.svg" alt="Bluetooth" width="140" height="140">
            <h2>It's wireless</h2>
            <p>To display the analysed signal and its features, signal data has to be sent from Feritoskop device (hardware part) to 
              mobile device (Android app). For that purpose, <b>Bluetooth</b> technology is being used to establish serial communication 
              between devices and to transfer the data in real-time over the 2.4 GHz wireless link</p>
            <p><a class="btn btn-secondary" href="about.html/#bluetooth" role="button">View details &raquo;</a></p>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <img  src="img/safety.svg" alt="Safety" width="140" height="140">
            <h2>It's safe</h2>
            <p>Unlike big, robust, old fashioned oscilloscopes, Feritoskop offers mobility and greater <b>safety</b> due to its wireless nature.
              Here, the device You use to measure the signal and the device where the signal is being analysed and shown, are 
              completely isolated from one another. It is also limited to low input voltage due to its non-commercial purpose</p>
            <p><a class="btn btn-secondary" href="about.html/#specifications" role="button">View details &raquo;</a></p>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <img src="img/easy.svg" alt="Like" width="140" height="140">
            <h2>It's easy to use</h2>
            <p>Users can study and visualize the signal data much more <b>easily</b> due to various Android OS features. 
              Feritoskop provides users with insight into oscilloscope - signal analysis and measurement, using a 
              familiar device - Android phone or tablet and thus it is perfect for students who are just getting into the world of electric engineering</p>
            <p><a class="btn btn-secondary" href="download.html/#instructions" role="button">View details &raquo;</a></p>
          </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->

        <!-- Specs -->

        <hr class="featurette-divider">

        <div class="row">
          <div class="col-md-7">
            <h2 class="featurette-heading">What is Feritoskop? <span class="text-muted">Except great...</span></h2>
            
            <p class="lead"><br><b>Feritoskop</b> is a device, built within <a href="http://pro-student.ferit.hr/">Pro-student</a> project 
              from 2018, that represents a wireless oscilloscope with a corresponding <b>Android</b> application that is adapted to use 
              on a tablet. The signal to be analyzed is sent to the device with the <b>Arduino</b> microprocessor (hardware part of the 
              Feritoskop) that processes it and displays it via the Bluetooth serial communication on the tablet device using the Feritoskop 
              Android application (software part of the project). The application provides the user with the most important function for 
              signal display and analysis as well as for ordinary oscilloscope.
            </p>
          </div>
          <div class="col-md-5">
            <img class="img-fluid mx-auto" src="img/feritoskop-transparent.png" alt="Feritoskop image">
            <p></p>
            <img class="img-fluid mx-auto" src="img/tablet.png" alt="Feritoskop image">
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row">
          <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">Simple hardware. <span class="text-muted">But powerful.</span></h2>
            <p class="lead"><br>Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod
              semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
              commodo.</p>
          </div>
          <div class="col-md-5 order-md-1">
            <img class="featurette-image img-fluid mx-auto" src="img/feritoskop-hausing.png" alt="Feritoskop image">
            <p></p>
            <img class="featurette-image img-fluid mx-auto" src="img/feritoskop-hardware.png" alt="Feritoskop image">
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row">
          <div class="col-md-7">
            <h2 class="featurette-heading">Beautiful software. <span class="text-muted">UI/UX done with style.</span></h2>
            <p class="lead"><br>Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod
              semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
              commodo.</p>
          </div>
          <div class="col-md-5">
            <img class="img-fluid mx-auto" src="img/tablet.png" alt="Feritoskop image">
            <p></p>
            <img class="img-fluid mx-auto" src="img/tablet2.png" alt="Feritoskop image">
          </div>
        </div>

        <hr class="featurette-divider">

        <!-- /END THE FEATURETTES -->

      </div><!-- /.container -->

      <!-- FOOTER -->
      <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017-2018 FERITOSKOP</p>
      </footer>

    </main>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js"></script>
    <script src="js/home.js"></script>
  </body>
</html>
