<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/feritoskop.ico">

    <title>Feritoskop</title>

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

  </head>

  <body>

    <header>

      <!--NAVIGATION BAR-->
      <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white">
        <img class="img-logo" href="index.php" src="img/feritoskop-logo.png">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.php">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="reviews.php">Reviews</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <main role="main">

      <!--Contact cover-->
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/bg-image.png" alt="Feritoskop background">
            <div class="container">
              <div class="carousel-caption text-right" style="background-color: #00223e5b; padding: 1rem; border-radius: 5px;">
                <p>Home -> Contact</p>
                <h1>Contact Feritoskop team</h1>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- FEATURES -->

      <div class="container marketing">

        <!--CONTACT FORM -->

        <div class="container">
          <div class="row">
          <h1 >Contact us!<span class="text-muted"> Let's get in touch</span></h1>
          <table width="100%" style="margin-top: 1.5rem">
            <div class="col-md-9 col-md-offset-0">
              <tr><td width="77%">
              <div class="form-container">

                <form id="contact-form" method = "post">

                  <fieldset>
                    <!-- Name input-->
                    <div class="form-group">
                      <label class="col-md-3 control-label" for="name">Your Name</label>
                      <div class="col-md-9">
                        <input id="name" name="name" type="text" placeholder="Enter your full name" class="form-control" required="required">
                      </div>
                    </div>
            
                    <!-- Email input-->
                    <div class="form-group">
                      <label class="col-md-3 control-label" for="email">Your E-mail</label>
                      <div class="col-md-9">
                        <input id="email" name="email" type="email" placeholder="Enter your e-mail" class="form-control" required="required">
                      </div>
                    </div>
            
                    <!-- Message body -->
                    <div class="form-group">
                      <label class="col-md-3 control-label" for="message">Your Message</label>
                      <div class="col-md-9">
                        <textarea class="form-control" id="message" name="message" placeholder="Enter your question or comment here..." rows="2" required="required"></textarea>
                      </div>

                    <!-- Form actions -->

                    <div class="form-group submit-container" style="margin-top: 3rem">
                      <div class="col-md-9">
                        <button id="submit" type="submit" class="btn mr-2 mail-btn">Submit</button>
                        <button id="clear" type="reset" class="btn btn-default btn-md">Clear</button>
                        <div id="spinner" class="spinner" style="display:none;">
                          <img id="img-spinner" src="assets/spinner.gif" alt="Loading"/>
                        </div>
                        <div id="error_message" class="text-danger form-message"></div>  
                        <div id="success_message" class="text-success form-message" style="margin-top: 1rem"></div> 
                      </div>
                    </div>

                  </fieldset>
                </form>
              </div>
            </div>
            </td>
            </tr>
            </table>
        </div>
        <!-- REVIEW FORM END -->

      </div><!-- /.container -->

      <!-- FOOTER -->
      <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017-2018 FERITOSKOP</p>
      </footer>

    </main>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js"></script>
    <script src="js/contact.js"></script>
  </body>
</html>
