<?php
    include "connection.php";
    $query_get = "SELECT name, email, message, rating FROM reviews WHERE allow = 1 ORDER BY id DESC";
    $result = mysqli_query($conn, $query_get);
    
    if(mysqli_num_rows($result)>0){
        while($row = mysqli_fetch_assoc($result)){
            echo 
            "<div class=\"testimonials-single\">
                <img src=\"assets/review.svg\" alt=\"client\" class=\"client-img\">
                <blockquote class=\"blockquote\">".$row['message']."</blockquote>
                <h5 class=\"mt-4 mb-2\">".$row['name']."</h5>
                <p class=\"text-primary\">".$row['email']."</p>
                <h6>".$row['rating']."/5</h6>
                <img class=\"rating\"src=\"assets/ratings/rating".$row['rating'].".png\">
            </div>";
        }
    }
    else echo "No user feedback yet :(";	
?>